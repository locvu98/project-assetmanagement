<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix ="form"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>




<%@ include file="head.jsp"%>

<%@ include file="header.jsp"%>

<div class="container main-content form">
	<div class="row">
		<form:form action="/DemoSpringMvc/asset/update" method="POST" modelAttribute="asset" >
			
			<form:hidden path="id" />
			
			<div class="form-group">
				<label>Name:</label> 
				<form:input path="name" class="form-control" />
			</div>
			
			<div class="form-group">
				<label>Quantity</label> 
				<form:input path="quantity" class="form-control" />
			</div>
			<button type="submit" class="btn btn-primary">Save</button>
		</form:form>
	</div>
</div>