<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>




<%@ include file="head.jsp"%>

<%@ include file="header.jsp"%>

<div class="container main-content form">
	<div class="row">
		<form:form action="/DemoSpringMvc/user/save" method="POST" commandName="user" >
			<input type="hidden" path="id" />
			<div class="form-group">
				<label>Full Name:</label> <input type="text" class="form-control"
					name="fullName" />
			</div>
			<div class="form-group">
				<label>Email Address:</label> <input type="text"
					class="form-control" name="email" />
			</div>
			<div class="form-group">
				<label>Phone Number:</label> <input type="text" class="form-control"
					name="phone" />
			</div>
			<button type="submit" class="btn btn-primary">Save</button>
		</form:form>
	</div>
</div>
<!-- /.container -->

<%@ include file="footer.jsp"%>
