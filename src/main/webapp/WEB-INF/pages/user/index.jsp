<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="head.jsp"%>

<%@ include file="header.jsp"%>

<div class="container main-content list">
	<h2>${success}</h2>
	<c:if test="${success != null && success != ''}">
		<div class="row alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<span>${success}</span>
		</div>
	</c:if>

	<div class="row">
		<a href="<c:url value="/user/create" />" class="btn btn-success pull-left"> <span
			class="glyphicon glyphicon-plus"></span> Add New User
		</a>
		<form class="form-inline pull-right" action="<c:url value="/user/search" />"
			method="GET">
			<div class="form-group">
				<input type="text" class="form-control" name="keyword"
					placeholder="Search user by name..." />
			</div>
			<button type="submit" class="btn btn-primary">Search</button>
		</form>
	</div>

	<c:choose>
		<c:when test="${users.isEmpty()}">
			<h3>No User Data Found !!!</h3>
		</c:when>
		<c:otherwise>
			<div class="row">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>No.</th>
							<th>Full Name</th>
							<th>Email Address</th>
							<th>Phone Number</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${users}" var="user" varStatus="loop">
							<tr>
								<td><c:out value="${loop.count}" /></td>
								<td><c:out value="${user.fullName}" /></td>
								<td><c:out value="${user.email}" /></td>
								<td><c:out value="${user.phone}" /></td>
								<td><a href="<c:url value="/user/${user.id}/edit" />">
										<span class="glyphicon glyphicon-pencil"></span>
								</a></td>
								<td><a href="<c:url value="/user/${user.id}/delete" />"
									onclick="return confirm('Bạn có chắc chắn muốn xóa user này ko ?');">
										<span class="glyphicon glyphicon-trash"></span>
								</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</c:otherwise>
	</c:choose>
</div>
<!-- /.container -->

<%@ include file="footer.jsp"%>
