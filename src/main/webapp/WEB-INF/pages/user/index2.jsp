<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Danh sách User</title>
<style type="text/css">
	table {
		width: 800px;
		border-collapse: collapse;
		margin: 30px auto;
	}
	
	table th, table td {
		padding: 8px 12px;
	}
	
	table thead tr {
		background-color: dodgerblue;
		color: white;
	}
</style>
</head>
<body>
	<table border="1">
		<thead>
			<tr>
				<th>No.</th>
				<th>Full Name</th>
				<th>Email Address</th>
				<th>Phone Number</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${users}" var="user" varStatus="loop">
				<tr>
					<td align="center"><c:out value="${loop.count}" /></td>
					<td><c:out value="${user.fullName}" /></td>
					<td><c:out value="${user.email}" /></td>
					<td align="center"><c:out value="${user.phone}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>