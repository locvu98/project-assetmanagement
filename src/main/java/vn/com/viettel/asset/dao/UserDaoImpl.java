package vn.com.viettel.asset.dao;

import java.util.List;

//import org.hibernate.Criteria;
//import org.hibernate.criterion.Restrictions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import vn.com.viettel.asset.entity.User;

public class UserDaoImpl implements UserDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public List<User> searchAll() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User order by n_id desc");

        return query.list();
    }

    @Override
//    public User searchById(long id) {
//        Session session = sessionFactory.getCurrentSession();
//
//        Criteria criteria = session.createCriteria(User.class);
//        criteria.add(Restrictions.eq("id", id));
//        return ((User) criteria.uniqueResult());
//    }
    public User searchById(long id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where n_id = " + id);

        return ((User) query.list().get(0));
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> searchByKeyword(String keyword) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where lower(s_full_name) like '%" + keyword + "%' order by n_id desc");

        return query.list();
    }

    @Override
    public void save(User data) {
        User user = new User();
//        user.setId(1000);
        user.setFullName(data.getFullName());
        user.setEmail(data.getEmail());
        user.setPhone(data.getPhone());

        Session session = sessionFactory.getCurrentSession();
        session.persist(user);
    }

    @Override
    public void delete(long id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete from User where n_id = " + id);
        query.executeUpdate();
    }   
}
