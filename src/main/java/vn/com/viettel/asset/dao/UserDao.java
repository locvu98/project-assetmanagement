package vn.com.viettel.asset.dao;

import java.util.List;

import vn.com.viettel.asset.entity.User;

public interface UserDao {
    List<User> searchAll();

    User searchById(long id);

    List<User> searchByKeyword(String keyword);

    void save(User data); //void save(UserModel data);

    void delete(long id);
}
