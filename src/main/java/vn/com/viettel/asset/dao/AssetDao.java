package vn.com.viettel.asset.dao;

import java.util.List;

import vn.com.viettel.asset.entity.Asset;


public interface AssetDao {
	 List<Asset> searchAll();
	 
	 void save(Asset data);
	 
	 List<Asset> searchByKeyword(String keyword);
	 
	 Asset searchById(long id);
	 
	 void delete(long id);
	 
	 void update(Asset data);
	 
	 Asset getAsset(long id);
}
