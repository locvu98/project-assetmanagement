package vn.com.viettel.asset.dao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import vn.com.viettel.asset.entity.Asset;

public class AssetDaoImpl implements AssetDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public List<Asset> searchAll() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Asset order by n_id desc");
        return query.list();
    }
    
    @Override
    public void save(Asset data) {
       
//    	Asset asset = new Asset();
//        asset.setName(data.getName());
//        asset.setQuantity(data.getQuantity());
        Session session = sessionFactory.getCurrentSession();
        //session.persist(asset);
        session.saveOrUpdate(data);
        //session.saveOrUpdate(asset);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Asset> searchByKeyword(String keyword) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Asset where (lower(s_name) like '%" + keyword + "%' or s_name like '%" + keyword + "%') order by n_id desc");

        return query.list();
    }
    
    @Override
    public Asset searchById(long id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Asset where n_id = " + id);
        return ((Asset) query.list().get(0));
    }
    
    @Override
    public void delete(long id) {
        Session session = sessionFactory.getCurrentSession();
        //Query query = session.createQuery("delete from Asset where n_id = " + id);
        //query.executeUpdate();
		Asset theAsset = session.byId(Asset.class).load(id);
		session.delete(theAsset);
    }   
    
    @Override
    public void update(Asset data) {
    	Session session = sessionFactory.getCurrentSession();
    	//Query query = session.createQuery("update Asset set n_quantity = " + data.getQuantity() +
    			//", s_name = " + data.getName() +
    			//" where n_id = " + data.getId());
    	session.evict(data);
    	session.save(data);
    }
    
    @Override
    public Asset getAsset(long id) {
    	Session currentSession = sessionFactory.getCurrentSession();
		Asset theAsset = currentSession.get(Asset.class, id);
		return theAsset;
    }
}
