package vn.com.viettel.asset.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_USER")
public class User implements Serializable {
    private static final long serialVersionUID = -3716673535008140879L;
    private long id;
    private String fullName;
    private String email;
    private String phone;

    public User() {
        super();
    }

    public User(long id, String fullName, String email, String phone) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
    }

    @Id
	
	 @SequenceGenerator(name = "sequenceNumberGenerator", sequenceName = "SEQ_ID",
	 allocationSize = 1)
	 
	 @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
	 "sequenceNumberGenerator")
	 
    @Column(name = "N_ID", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "S_FULL_NAME", nullable = false)
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Column(name = "S_EMAIL", nullable = false)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "S_PHONE")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
