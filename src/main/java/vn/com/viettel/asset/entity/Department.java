package vn.com.viettel.asset.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_DEPARTMENT")

public class Department implements Serializable {
    private static final long serialVersionUID = -3716673535008140879L;
    private long id;
    private String name;
	public Department() {
		super();
	}
	public Department(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	@Id
	@SequenceGenerator(name = "sequenceNumberGenerator", sequenceName = "SEQ_ID",
	 allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
	 "sequenceNumberGenerator")
	@Column(name = "N_ID", nullable = false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "S_NAME", nullable = false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
    
}
