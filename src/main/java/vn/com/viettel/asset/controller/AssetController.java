package vn.com.viettel.asset.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import vn.com.viettel.asset.dao.AssetDao;
import vn.com.viettel.asset.entity.Asset;

@Controller
@Transactional
public class AssetController {
	@Autowired
	private AssetDao assetDao;
	
	@RequestMapping(value = "/asset", method = RequestMethod.GET) 
	public String index(Model model) {
        model.addAttribute("assets", assetDao.searchAll());
        return "asset/index";
    }
	
	@RequestMapping(value="/asset/create", method = RequestMethod.GET)
	public String create(Model model) {

		model.addAttribute("asset", new Asset());
		return "asset/save";
	}
	
	@RequestMapping(value="/asset/save", method = RequestMethod.POST)
	public String save(Asset asset, RedirectAttributes redirect) {
		assetDao.save(asset);
        redirect.addFlashAttribute("success", "You've just saved asset info successfully !");
		return "redirect:/asset";
	}
	
    @RequestMapping(value = "/asset/search", method = RequestMethod.GET)
    public String search(@RequestParam("keyword") String keyword, Model model) {
        if ("".equals(keyword)) {
            return "redirect:/asset";
        }

        model.addAttribute("assets", assetDao.searchByKeyword(keyword));

        return "asset/index";
    }
    
    @RequestMapping(value = "/asset/{id}/delete", method = RequestMethod.GET)
    public String delete(@PathVariable long id, RedirectAttributes redirect) {
       // Asset asset = assetDao.searchById(id);
        assetDao.delete(id);
        redirect.addFlashAttribute("success", "You've just deleted asset info successfully !");

        return "redirect:/asset";
    }
    
//    @RequestMapping(value = "/asset/{id}/edit", method = RequestMethod.GET)
//    public String edit(@PathVariable long id, Model model) {
//        model.addAttribute("asset", assetDao.searchById(id));
//        
//        return "asset/edit";
//    }
    
	@RequestMapping(value="/asset/update", method = RequestMethod.POST)
	public String update(Asset asset, RedirectAttributes redirect) {
		assetDao.update(asset);
        redirect.addFlashAttribute("success", "You've just updated asset info successfully !");
		return "redirect:/asset";
	}
    
	   @RequestMapping(value = "/asset/{id}/edit", method = RequestMethod.GET)
	    public String edit(@PathVariable long id, Model model) {
	       Asset theAsset = assetDao.getAsset(id);
		   model.addAttribute("asset", theAsset);
	       return "asset/save";
	    }
    
}
