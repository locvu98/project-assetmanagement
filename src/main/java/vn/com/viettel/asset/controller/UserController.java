package vn.com.viettel.asset.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import vn.com.viettel.asset.dao.UserDao;
import vn.com.viettel.asset.entity.User;
//import vn.com.viettel.asset.model.UserModel;

@Controller
@Transactional
public class UserController {
    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("users", userDao.searchAll());

        return "user/index";
    }

    @RequestMapping(value = "/user/search", method = RequestMethod.GET)
    public String search(@RequestParam("keyword") String keyword, Model model) {
        if ("".equals(keyword)) {
            return "redirect:/user";
        }

        model.addAttribute("users", userDao.searchByKeyword(keyword));

        return "user/index";
    }

    @RequestMapping(value = "/user/create", method = RequestMethod.GET)
    public String create(Model model) {
        model.addAttribute("user", new User());

        return "user/save";
    }

    @RequestMapping(value = "/user/{id}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable long id, Model model) {
        model.addAttribute("user", userDao.searchById(id));

        return "user/save";
    }

    @RequestMapping(value = "/user/save", method = RequestMethod.POST)
    public String save(User user, RedirectAttributes redirect) {
		userDao.save(user);
        redirect.addFlashAttribute("success", "You've just saved user info successfully !");

        return "redirect:/user";
    }

    @RequestMapping(value = "/user/{id}/delete", method = RequestMethod.GET)
    public String delete(@PathVariable long id, RedirectAttributes redirect) {
        User user = userDao.searchById(id);
        userDao.delete(id);
        redirect.addFlashAttribute("success", "You've just deleted user info successfully !");

        return "redirect:/user";
    }
}
